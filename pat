Common subdirectories: ./.git and ../linux-2.6/.git
Only in ../linux-2.6/: .gitignore
Only in .: .oldconfig
diff -C 5 ./CREDITS ../linux-2.6/CREDITS
*** ./CREDITS	Sun Feb 17 04:23:05 2008
--- ../linux-2.6/CREDITS	Thu Mar  6 19:15:06 2008
***************
*** 506,521 ****
  E: m@mbsks.franken.de
  D: REINER SCT cyberJack pinpad/e-com USB chipcard reader driver
  S: Germany
  
  N: Adrian Bunk
- E: bunk@stusta.de
  P: 1024D/4F12B400  B29C E71E FE19 6755 5C8A  84D4 99FC EA98 4F12 B400
  D: misc kernel hacking and testing
- S: Grasmeierstrasse 11
- S: 80805 Muenchen
- S: Germany
  
  N: Ray Burr
  E: ryb@nightmare.com
  D: Original author of Amiga FFS filesystem
  S: Orlando, Florida
--- 506,517 ----
***************
*** 1122,1131 ****
--- 1118,1130 ----
  D: AdvanSys SCSI driver
  S: 1150 Ringwood Court
  S: San Jose, California 95131
  S: USA
  
+ N: Adam Fritzler
+ E: mid@zigamorph.net
+ 
  N: Fernando Fuganti
  E: fuganti@conectiva.com.br
  E: fuganti@netbank.com.br
  D: random kernel hacker, ZF MachZ Watchdog driver
  S: Conectiva S.A.
***************
*** 1351,1360 ****
--- 1350,1367 ----
  D: MTRR emulation with Centaur MCRs
  S: Gen Stedmanstraat 212
  S: 5623 HZ Eindhoven
  S: The Netherlands
  
+ N: Oliver Hartkopp
+ E: oliver.hartkopp@volkswagen.de
+ W: http://www.volkswagen.de
+ D: Controller Area Network (network layer core)
+ S: Brieffach 1776
+ S: 38436 Wolfsburg
+ S: Germany
+ 
  N: Andrew Haylett
  E: ajh@primag.co.uk
  D: Selection mechanism
  
  N: Andre Hedrick
***************
*** 3304,3313 ****
--- 3311,3328 ----
  S: IRISA
  S: Universit=E9 de Rennes I
  S: F-35042 Rennes Cedex
  S: France
  
+ N: Urs Thuermann
+ E: urs.thuermann@volkswagen.de
+ W: http://www.volkswagen.de
+ D: Controller Area Network (network layer core)
+ S: Brieffach 1776
+ S: 38436 Wolfsburg
+ S: Germany
+ 
  N: Jon Tombs
  E: jon@gte.esi.us.es
  W: http://www.esi.us.es/~jon
  D: NFS mmap()
  D: XF86_S3
Common subdirectories: ./Documentation and ../linux-2.6/Documentation
diff -C 5 ./MAINTAINERS ../linux-2.6/MAINTAINERS
*** ./MAINTAINERS	Sun Feb 17 04:22:44 2008
--- ../linux-2.6/MAINTAINERS	Thu Mar  6 19:15:06 2008
***************
*** 638,649 ****
  S:	Maintained
  
  ASYNCHRONOUS TRANSFERS/TRANSFORMS API
  P:	Dan Williams
  M:	dan.j.williams@intel.com
! P:	Shannon Nelson
! M:	shannon.nelson@intel.com
  L:	linux-kernel@vger.kernel.org
  W:	http://sourceforge.net/projects/xscaleiop
  S:	Supported
  
  ATA OVER ETHERNET DRIVER
--- 638,649 ----
  S:	Maintained
  
  ASYNCHRONOUS TRANSFERS/TRANSFORMS API
  P:	Dan Williams
  M:	dan.j.williams@intel.com
! P:	Maciej Sosnowski
! M:	maciej.sosnowski@intel.com
  L:	linux-kernel@vger.kernel.org
  W:	http://sourceforge.net/projects/xscaleiop
  S:	Supported
  
  ATA OVER ETHERNET DRIVER
***************
*** 695,705 ****
  S:	Supported
  
  ATMEL LCDFB DRIVER
  P:	Nicolas Ferre
  M:	nicolas.ferre@atmel.com
! L:	linux-fbdev-devel@lists.sourceforge.net (subscribers-only)
  S:	Maintained
  
  ATMEL MACB ETHERNET DRIVER
  P:	Haavard Skinnemoen
  M:	hskinnemoen@atmel.com
--- 695,705 ----
  S:	Supported
  
  ATMEL LCDFB DRIVER
  P:	Nicolas Ferre
  M:	nicolas.ferre@atmel.com
! L:	linux-fbdev-devel@lists.sourceforge.net (moderated for non-subscribers)
  S:	Maintained
  
  ATMEL MACB ETHERNET DRIVER
  P:	Haavard Skinnemoen
  M:	hskinnemoen@atmel.com
***************
*** 765,782 ****
  M:	rpurdie@rpsys.net
  S:	Maintained
  
  BLACKFIN ARCHITECTURE
  P:	Bryan Wu
! M:	bryan.wu@analog.com
  L:	uclinux-dist-devel@blackfin.uclinux.org (subscribers-only)
  W:	http://blackfin.uclinux.org
  S:	Supported
  
  BLACKFIN EMAC DRIVER
  P:	Bryan Wu
! M:	bryan.wu@analog.com
  L:	uclinux-dist-devel@blackfin.uclinux.org (subscribers-only)
  W:	http://blackfin.uclinux.org
  S:	Supported
  
  BLACKFIN RTC DRIVER
--- 765,782 ----
  M:	rpurdie@rpsys.net
  S:	Maintained
  
  BLACKFIN ARCHITECTURE
  P:	Bryan Wu
! M:	cooloney@kernel.org
  L:	uclinux-dist-devel@blackfin.uclinux.org (subscribers-only)
  W:	http://blackfin.uclinux.org
  S:	Supported
  
  BLACKFIN EMAC DRIVER
  P:	Bryan Wu
! M:	cooloney@kernel.org
  L:	uclinux-dist-devel@blackfin.uclinux.org (subscribers-only)
  W:	http://blackfin.uclinux.org
  S:	Supported
  
  BLACKFIN RTC DRIVER
***************
*** 980,989 ****
--- 980,995 ----
  P:	Michael Chan
  M:	mchan@broadcom.com
  L:	netdev@vger.kernel.org
  S:	Supported
  
+ BROADCOM BNX2X 10 GIGABIT ETHERNET DRIVER
+ P:	Eliezer Tamir
+ M:	eliezert@broadcom.com
+ L:	netdev@vger.kernel.org
+ S:	Supported
+ 
  BROADCOM TG3 GIGABIT ETHERNET DRIVER
  P:	Michael Chan
  M:	mchan@broadcom.com
  L:	netdev@vger.kernel.org
  S:	Supported
***************
*** 1130,1139 ****
--- 1136,1151 ----
  M:	cxacru@fire.lp0.eu
  L:	accessrunner-general@lists.sourceforge.net
  W:	http://accessrunner.sourceforge.net/
  S:	Maintained
  
+ CONTROL GROUPS (CGROUPS)
+ P:	Paul Menage
+ M:	menage@google.com
+ L:	containers@lists.linux-foundation.org
+ S:	Maintained
+ 
  CORETEMP HARDWARE MONITORING DRIVER
  P:	Rudolf Marek
  M:	r.marek@assembler.cz
  L:	lm-sensors@lm-sensors.org
  S:	Maintained
***************
*** 1200,1210 ****
  S:	Maintained
  
  CYBLAFB FRAMEBUFFER DRIVER
  P:	Knut Petersen
  M:	Knut_Petersen@t-online.de
! L:	linux-fbdev-devel@lists.sourceforge.net (subscribers-only)
  S:	Maintained
  
  CYCLADES 2X SYNC CARD DRIVER
  P:	Arnaldo Carvalho de Melo
  M:	acme@ghostprotocols.net
--- 1212,1222 ----
  S:	Maintained
  
  CYBLAFB FRAMEBUFFER DRIVER
  P:	Knut Petersen
  M:	Knut_Petersen@t-online.de
! L:	linux-fbdev-devel@lists.sourceforge.net (moderated for non-subscribers)
  S:	Maintained
  
  CYCLADES 2X SYNC CARD DRIVER
  P:	Arnaldo Carvalho de Melo
  M:	acme@ghostprotocols.net
***************
*** 1322,1344 ****
  M:	ccaulfie@redhat.com
  P:	David Teigland
  M:	teigland@redhat.com
  L:	cluster-devel@redhat.com
  W:	http://sources.redhat.com/cluster/
! T:	git kernel.org:/pub/scm/linux/kernel/git/steve/gfs2-2.6-fixes.git
! T:	git kernel.org:/pub/scm/linux/kernel/git/steve/gfs2-2.6-nmw.git
  S:	Supported
  
  DAVICOM FAST ETHERNET (DMFE) NETWORK DRIVER
  P:	Tobias Ringstrom
  M:	tori@unhappy.mine.nu
  L:	netdev@vger.kernel.org
  S:	Maintained
  
  DMA GENERIC OFFLOAD ENGINE SUBSYSTEM
! P:	Shannon Nelson
! M:	shannon.nelson@intel.com
  P:	Dan Williams
  M:	dan.j.williams@intel.com
  L:	linux-kernel@vger.kernel.org
  S:	Supported
  
--- 1334,1355 ----
  M:	ccaulfie@redhat.com
  P:	David Teigland
  M:	teigland@redhat.com
  L:	cluster-devel@redhat.com
  W:	http://sources.redhat.com/cluster/
! T:	git kernel.org:/pub/scm/linux/kernel/git/teigland/dlm.git
  S:	Supported
  
  DAVICOM FAST ETHERNET (DMFE) NETWORK DRIVER
  P:	Tobias Ringstrom
  M:	tori@unhappy.mine.nu
  L:	netdev@vger.kernel.org
  S:	Maintained
  
  DMA GENERIC OFFLOAD ENGINE SUBSYSTEM
! P:	Maciej Sosnowski
! M:	maciej.sosnowski@intel.com
  P:	Dan Williams
  M:	dan.j.williams@intel.com
  L:	linux-kernel@vger.kernel.org
  S:	Supported
  
***************
*** 1578,1591 ****
  S:	Supported
  
  FRAMEBUFFER LAYER
  P:	Antonino Daplas
  M:	adaplas@gmail.com
! L:	linux-fbdev-devel@lists.sourceforge.net (subscribers-only)
  W:	http://linux-fbdev.sourceforge.net/
  S:	Maintained
  
  FREESCALE SOC FS_ENET DRIVER
  P:	Pantelis Antoniou
  M:	pantelis.antoniou@gmail.com
  P:	Vitaly Bordug
  M:	vbordug@ru.mvista.com
--- 1589,1609 ----
  S:	Supported
  
  FRAMEBUFFER LAYER
  P:	Antonino Daplas
  M:	adaplas@gmail.com
! L:	linux-fbdev-devel@lists.sourceforge.net (moderated for non-subscribers)
  W:	http://linux-fbdev.sourceforge.net/
  S:	Maintained
  
+ FREESCALE DMA DRIVER
+ P;	Zhang Wei
+ M:	wei.zhang@freescale.com
+ L:	linuxppc-embedded@ozlabs.org
+ L:	linux-kernel@vger.kernel.org
+ S:	Maintained
+ 
  FREESCALE SOC FS_ENET DRIVER
  P:	Pantelis Antoniou
  M:	pantelis.antoniou@gmail.com
  P:	Vitaly Bordug
  M:	vbordug@ru.mvista.com
***************
*** 1711,1723 ****
  T:	git lm-sensors.org:/kernel/mhoffman/hwmon-2.6.git testing
  T:	git lm-sensors.org:/kernel/mhoffman/hwmon-2.6.git release
  S:	Maintained
  
  HARDWARE RANDOM NUMBER GENERATOR CORE
! P:	Michael Buesch
! M:	mb@bu3sch.de
! S:	Maintained
  
  HARD DRIVE ACTIVE PROTECTION SYSTEM (HDAPS) DRIVER
  P:	Robert Love
  M:	rlove@rlove.org
  M:	linux-kernel@vger.kernel.org
--- 1729,1739 ----
  T:	git lm-sensors.org:/kernel/mhoffman/hwmon-2.6.git testing
  T:	git lm-sensors.org:/kernel/mhoffman/hwmon-2.6.git release
  S:	Maintained
  
  HARDWARE RANDOM NUMBER GENERATOR CORE
! S:	Orphaned
  
  HARD DRIVE ACTIVE PROTECTION SYSTEM (HDAPS) DRIVER
  P:	Robert Love
  M:	rlove@rlove.org
  M:	linux-kernel@vger.kernel.org
***************
*** 1922,1932 ****
  T:	quilt kernel.org/pub/linux/kernel/people/bart/pata-2.6/
  S:	Maintained
  
  IDE/ATAPI CDROM DRIVER
  P:	Borislav Petkov
! M:	bbpetkov@yahoo.de
  L:	linux-ide@vger.kernel.org
  S:	Maintained
  
  IDE/ATAPI FLOPPY DRIVERS
  P:	Paul Bristow
--- 1938,1948 ----
  T:	quilt kernel.org/pub/linux/kernel/people/bart/pata-2.6/
  S:	Maintained
  
  IDE/ATAPI CDROM DRIVER
  P:	Borislav Petkov
! M:	petkovbb@gmail.com
  L:	linux-ide@vger.kernel.org
  S:	Maintained
  
  IDE/ATAPI FLOPPY DRIVERS
  P:	Paul Bristow
***************
*** 1963,1973 ****
  M:	stefanr@s5r6.in-berlin.de
  L:	linux1394-devel@lists.sourceforge.net
  S:	Maintained
  
  IMS TWINTURBO FRAMEBUFFER DRIVER
! L:	linux-fbdev-devel@lists.sourceforge.net (subscribers-only)
  S:	Orphan
  
  INFINIBAND SUBSYSTEM
  P:	Roland Dreier
  M:	rolandd@cisco.com
--- 1979,1989 ----
  M:	stefanr@s5r6.in-berlin.de
  L:	linux1394-devel@lists.sourceforge.net
  S:	Maintained
  
  IMS TWINTURBO FRAMEBUFFER DRIVER
! L:	linux-fbdev-devel@lists.sourceforge.net (moderated for non-subscribers)
  S:	Orphan
  
  INFINIBAND SUBSYSTEM
  P:	Roland Dreier
  M:	rolandd@cisco.com
***************
*** 1997,2023 ****
  S:	Maintained
  
  INTEL FRAMEBUFFER DRIVER (excluding 810 and 815)
  P:	Sylvain Meyer
  M:	sylvain.meyer@worldonline.fr
! L:	linux-fbdev-devel@lists.sourceforge.net (subscribers-only)
  S:	Maintained
  
  INTEL 810/815 FRAMEBUFFER DRIVER
  P:	Antonino Daplas
  M:	adaplas@gmail.com
! L:	linux-fbdev-devel@lists.sourceforge.net (subscribers-only)
  S:	Maintained
  
  INTEL IA32 MICROCODE UPDATE SUPPORT
  P:	Tigran Aivazian
  M:	tigran@aivazian.fsnet.co.uk
  S:	Maintained
  
  INTEL I/OAT DMA DRIVER
! P:	Shannon Nelson
! M:	shannon.nelson@intel.com
  L:	linux-kernel@vger.kernel.org
  S:	Supported
  
  INTEL IOP-ADMA DMA DRIVER
  P:	Dan Williams
--- 2013,2039 ----
  S:	Maintained
  
  INTEL FRAMEBUFFER DRIVER (excluding 810 and 815)
  P:	Sylvain Meyer
  M:	sylvain.meyer@worldonline.fr
! L:	linux-fbdev-devel@lists.sourceforge.net (moderated for non-subscribers)
  S:	Maintained
  
  INTEL 810/815 FRAMEBUFFER DRIVER
  P:	Antonino Daplas
  M:	adaplas@gmail.com
! L:	linux-fbdev-devel@lists.sourceforge.net (moderated for non-subscribers)
  S:	Maintained
  
  INTEL IA32 MICROCODE UPDATE SUPPORT
  P:	Tigran Aivazian
  M:	tigran@aivazian.fsnet.co.uk
  S:	Maintained
  
  INTEL I/OAT DMA DRIVER
! P:	Maciej Sosnowski
! M:	maciej.sosnowski@intel.com
  L:	linux-kernel@vger.kernel.org
  S:	Supported
  
  INTEL IOP-ADMA DMA DRIVER
  P:	Dan Williams
***************
*** 2599,2609 ****
  S:	Odd Fixes for 2.4; Maintained for 2.6.
  
  MATROX FRAMEBUFFER DRIVER
  P:	Petr Vandrovec
  M:	vandrove@vc.cvut.cz
! L:	linux-fbdev-devel@lists.sourceforge.net (subscribers-only)
  S:	Maintained
  
  MAX6650 HARDWARE MONITOR AND FAN CONTROLLER DRIVER
  P:	Hans J. Koch
  M:	hjk@linutronix.de
--- 2615,2625 ----
  S:	Odd Fixes for 2.4; Maintained for 2.6.
  
  MATROX FRAMEBUFFER DRIVER
  P:	Petr Vandrovec
  M:	vandrove@vc.cvut.cz
! L:	linux-fbdev-devel@lists.sourceforge.net (moderated for non-subscribers)
  S:	Maintained
  
  MAX6650 HARDWARE MONITOR AND FAN CONTROLLER DRIVER
  P:	Hans J. Koch
  M:	hjk@linutronix.de
***************
*** 2621,2630 ****
--- 2637,2657 ----
  L:	linux-mm@kvack.org
  L:	linux-kernel@vger.kernel.org
  W:	http://www.linux-mm.org
  S:	Maintained
  
+ MEMORY RESOURCE CONTROLLER
+ P:	Balbir Singh
+ M:	balbir@linux.vnet.ibm.com
+ P:	Pavel Emelyanov
+ M:	xemul@openvz.org
+ P:	KAMEZAWA Hiroyuki
+ M:	kamezawa.hiroyu@jp.fujitsu.com
+ L:	linux-mm@kvack.org
+ L:	linux-kernel@vger.kernel.org
+ S:	Maintained
+ 
  MEI MN10300/AM33 PORT
  P:	David Howells
  M:	dhowells@redhat.com
  P:	Koichi Yasutake
  M:	yasutake.koichi@jp.panasonic.com
***************
*** 2745,2754 ****
--- 2772,2783 ----
  S:	Maintained
  
  NETEFFECT IWARP RNIC DRIVER (IW_NES)
  P:	Faisal Latif
  M:	flatif@neteffect.com
+ P:	Nishi Gupta
+ M:	ngupta@neteffect.com
  P:	Glenn Streiff
  M:	gstreiff@neteffect.com
  L:	general@lists.openfabrics.org
  W:	http://www.neteffect.com
  S:	Supported
***************
*** 2908,2918 ****
  S:	Maintained
  
  NVIDIA (rivafb and nvidiafb) FRAMEBUFFER DRIVER
  P:	Antonino Daplas
  M:	adaplas@gmail.com
! L:	linux-fbdev-devel@lists.sourceforge.net (subscribers-only)
  S:	Maintained
  
  OPENCORES I2C BUS DRIVER
  P:	Peter Korsgaard
  M:	jacmet@sunsite.dk
--- 2937,2947 ----
  S:	Maintained
  
  NVIDIA (rivafb and nvidiafb) FRAMEBUFFER DRIVER
  P:	Antonino Daplas
  M:	adaplas@gmail.com
! L:	linux-fbdev-devel@lists.sourceforge.net (moderated for non-subscribers)
  S:	Maintained
  
  OPENCORES I2C BUS DRIVER
  P:	Peter Korsgaard
  M:	jacmet@sunsite.dk
***************
*** 3237,3253 ****
  S:	Maintained
  
  RADEON FRAMEBUFFER DISPLAY DRIVER
  P:	Benjamin Herrenschmidt
  M:	benh@kernel.crashing.org
! L:	linux-fbdev-devel@lists.sourceforge.net (subscribers-only)
  S:	Maintained
  
  RAGE128 FRAMEBUFFER DISPLAY DRIVER
  P:	Paul Mackerras
  M:	paulus@samba.org
! L:	linux-fbdev-devel@lists.sourceforge.net (subscribers-only)
  S:	Maintained
  
  RAYLINK/WEBGEAR 802.11 WIRELESS LAN DRIVER
  P:	Corey Thomas
  M:	coreythomas@charter.net
--- 3266,3282 ----
  S:	Maintained
  
  RADEON FRAMEBUFFER DISPLAY DRIVER
  P:	Benjamin Herrenschmidt
  M:	benh@kernel.crashing.org
! L:	linux-fbdev-devel@lists.sourceforge.net (moderated for non-subscribers)
  S:	Maintained
  
  RAGE128 FRAMEBUFFER DISPLAY DRIVER
  P:	Paul Mackerras
  M:	paulus@samba.org
! L:	linux-fbdev-devel@lists.sourceforge.net (moderated for non-subscribers)
  S:	Maintained
  
  RAYLINK/WEBGEAR 802.11 WIRELESS LAN DRIVER
  P:	Corey Thomas
  M:	coreythomas@charter.net
***************
*** 3348,3358 ****
  S:	Maintained
  
  S3 SAVAGE FRAMEBUFFER DRIVER
  P:	Antonino Daplas
  M:	adaplas@gmail.com
! L:	linux-fbdev-devel@lists.sourceforge.net (subscribers-only)
  S:	Maintained
  
  S390
  P:	Martin Schwidefsky
  M:	schwidefsky@de.ibm.com
--- 3377,3387 ----
  S:	Maintained
  
  S3 SAVAGE FRAMEBUFFER DRIVER
  P:	Antonino Daplas
  M:	adaplas@gmail.com
! L:	linux-fbdev-devel@lists.sourceforge.net (moderated for non-subscribers)
  S:	Maintained
  
  S390
  P:	Martin Schwidefsky
  M:	schwidefsky@de.ibm.com
***************
*** 3885,3898 ****
  P:	Jesper Juhl
  M:	trivial@kernel.org
  L:	linux-kernel@vger.kernel.org
  S:	Maintained
  
! TULIP NETWORK DRIVER
! L:	tulip-users@lists.sourceforge.net
! W:	http://sourceforge.net/projects/tulip/
! S:	Orphan
  
  TUN/TAP driver
  P:	Maxim Krasnyansky
  M:	maxk@qualcomm.com
  L:	vtun@office.satix.net
--- 3914,3930 ----
  P:	Jesper Juhl
  M:	trivial@kernel.org
  L:	linux-kernel@vger.kernel.org
  S:	Maintained
  
! TULIP NETWORK DRIVERS
! P:	Grant Grundler
! M:	grundler@parisc-linux.org
! P:	Kyle McMartin
! M:	kyle@parisc-linux.org
! L:	netdev@vger.kernel.org
! S:	Maintained
  
  TUN/TAP driver
  P:	Maxim Krasnyansky
  M:	maxk@qualcomm.com
  L:	vtun@office.satix.net
***************
*** 4260,4270 ****
  L:	lm-sensors@lm-sensors.org
  S:	Maintained
  
  VT8231 HARDWARE MONITOR DRIVER
  P:	Roger Lucas
! M:	roger@planbit.co.uk
  L:	lm-sensors@lm-sensors.org
  S:	Maintained
  
  W1 DALLAS'S 1-WIRE BUS
  P:	Evgeniy Polyakov
--- 4292,4302 ----
  L:	lm-sensors@lm-sensors.org
  S:	Maintained
  
  VT8231 HARDWARE MONITOR DRIVER
  P:	Roger Lucas
! M:	vt8231@hiddenengine.co.uk
  L:	lm-sensors@lm-sensors.org
  S:	Maintained
  
  W1 DALLAS'S 1-WIRE BUS
  P:	Evgeniy Polyakov
diff -C 5 ./Makefile ../linux-2.6/Makefile
*** ./Makefile	Tue Feb 19 19:36:20 2008
--- ../linux-2.6/Makefile	Thu Mar  6 19:15:06 2008
***************
*** 1,9 ****
  VERSION = 2
  PATCHLEVEL = 6
  SUBLEVEL = 25
! EXTRAVERSION = -rc2-git1
  NAME = Funky Weasel is Jiggy wit it
  
  # *DOCUMENTATION*
  # To see a list of typical targets execute "make help"
  # More info can be located in ./README
--- 1,9 ----
  VERSION = 2
  PATCHLEVEL = 6
  SUBLEVEL = 25
! EXTRAVERSION = -rc4
  NAME = Funky Weasel is Jiggy wit it
  
  # *DOCUMENTATION*
  # To see a list of typical targets execute "make help"
  # More info can be located in ./README
***************
*** 505,514 ****
--- 505,518 ----
  KBUILD_CFLAGS	+= -Os
  else
  KBUILD_CFLAGS	+= -O2
  endif
  
+ # Force gcc to behave correct even for buggy distributions
+ # Arch Makefiles may override this setting
+ KBUILD_CFLAGS += $(call cc-option, -fno-stack-protector)
+ 
  include $(srctree)/arch/$(SRCARCH)/Makefile
  
  ifdef CONFIG_FRAME_POINTER
  KBUILD_CFLAGS	+= -fno-omit-frame-pointer -fno-optimize-sibling-calls
  else
***************
*** 523,535 ****
  # We trigger additional mismatches with less inlining
  ifdef CONFIG_DEBUG_SECTION_MISMATCH
  KBUILD_CFLAGS += $(call cc-option, -fno-inline-functions-called-once)
  endif
  
- # Force gcc to behave correct even for buggy distributions
- KBUILD_CFLAGS         += $(call cc-option, -fno-stack-protector)
- 
  # arch Makefile may override CC so keep this after arch Makefile is included
  NOSTDINC_FLAGS += -nostdinc -isystem $(shell $(CC) -print-file-name=include)
  CHECKFLAGS     += $(NOSTDINC_FLAGS)
  
  # warn about C99 declaration after statement
--- 527,536 ----
***************
*** 808,818 ****
  	$(call vmlinux-modpost)
  	$(call if_changed_rule,vmlinux__)
  	$(Q)rm -f .old_version
  
  # build vmlinux.o first to catch section mismatch errors early
! $(kallsyms.o): vmlinux.o
  vmlinux.o: $(vmlinux-lds) $(vmlinux-init) $(vmlinux-main) FORCE
  	$(call if_changed_rule,vmlinux-modpost)
  
  # The actual objects are generated when descending, 
  # make sure no implicit rule kicks in
--- 809,821 ----
  	$(call vmlinux-modpost)
  	$(call if_changed_rule,vmlinux__)
  	$(Q)rm -f .old_version
  
  # build vmlinux.o first to catch section mismatch errors early
! ifdef CONFIG_KALLSYMS
! .tmp_vmlinux1: vmlinux.o
! endif
  vmlinux.o: $(vmlinux-lds) $(vmlinux-init) $(vmlinux-main) FORCE
  	$(call if_changed_rule,vmlinux-modpost)
  
  # The actual objects are generated when descending, 
  # make sure no implicit rule kicks in
Only in .: README.wrt350n
Common subdirectories: ./arch and ../linux-2.6/arch
Common subdirectories: ./block and ../linux-2.6/block
Common subdirectories: ./crypto and ../linux-2.6/crypto
Common subdirectories: ./drivers and ../linux-2.6/drivers
Common subdirectories: ./fs and ../linux-2.6/fs
Only in .: images
Common subdirectories: ./include and ../linux-2.6/include
Common subdirectories: ./init and ../linux-2.6/init
Common subdirectories: ./ipc and ../linux-2.6/ipc
Common subdirectories: ./kernel and ../linux-2.6/kernel
Common subdirectories: ./lib and ../linux-2.6/lib
Common subdirectories: ./mm and ../linux-2.6/mm
Common subdirectories: ./net and ../linux-2.6/net
Only in .: pat
Common subdirectories: ./samples and ../linux-2.6/samples
Common subdirectories: ./scripts and ../linux-2.6/scripts
Common subdirectories: ./security and ../linux-2.6/security
Common subdirectories: ./sound and ../linux-2.6/sound
Common subdirectories: ./usr and ../linux-2.6/usr
Common subdirectories: ./virt and ../linux-2.6/virt
