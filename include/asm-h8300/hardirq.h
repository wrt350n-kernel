#ifndef __H8300_HARDIRQ_H
#define __H8300_HARDIRQ_H

#include <linux/kernel.h>
#include <linux/threads.h>
#include <linux/interrupt.h>
#include <linux/irq.h>

typedef struct {
	unsigned int __softirq_pending;
} ____cacheline_aligned irq_cpustat_t;

#include <linux/irq_cpustat.h>	/* Standard mappings for irq_cpustat_t above */

<<<<<<< HEAD:include/asm-h8300/hardirq.h
=======
extern void ack_bad_irq(unsigned int irq);

>>>>>>> 264e3e889d86e552b4191d69bb60f4f3b383135a:include/asm-h8300/hardirq.h
#define HARDIRQ_BITS	8

/*
 * The hardirq mask has to be large enough to have
 * space for potentially all IRQ sources in the system
 * nesting on a single CPU:
 */
#if (1 << HARDIRQ_BITS) < NR_IRQS
# error HARDIRQ_BITS is too low!
#endif

#endif
