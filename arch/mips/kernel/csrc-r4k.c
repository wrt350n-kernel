/*
 * This file is subject to the terms and conditions of the GNU General Public
 * License.  See the file "COPYING" in the main directory of this archive
 * for more details.
 *
 * Copyright (C) 2007 by Ralf Baechle
 */
#include <linux/clocksource.h>
#include <linux/init.h>

#include <asm/time.h>

static cycle_t c0_hpt_read(void)
{
	return read_c0_count();
}

static struct clocksource clocksource_mips = {
	.name		= "MIPS",
	.read		= c0_hpt_read,
	.mask		= CLOCKSOURCE_MASK(32),
	.flags		= CLOCK_SOURCE_IS_CONTINUOUS,
};

<<<<<<< HEAD:arch/mips/kernel/csrc-r4k.c
void __init init_mips_clocksource(void)
=======
int __init init_mips_clocksource(void)
>>>>>>> 264e3e889d86e552b4191d69bb60f4f3b383135a:arch/mips/kernel/csrc-r4k.c
{
<<<<<<< HEAD:arch/mips/kernel/csrc-r4k.c
=======
	if (!cpu_has_counter || !mips_hpt_frequency)
		return -ENXIO;

>>>>>>> 264e3e889d86e552b4191d69bb60f4f3b383135a:arch/mips/kernel/csrc-r4k.c
	/* Calclate a somewhat reasonable rating value */
	clocksource_mips.rating = 200 + mips_hpt_frequency / 10000000;

	clocksource_set_clock(&clocksource_mips, mips_hpt_frequency);

	clocksource_register(&clocksource_mips);
<<<<<<< HEAD:arch/mips/kernel/csrc-r4k.c
=======

	return 0;
>>>>>>> 264e3e889d86e552b4191d69bb60f4f3b383135a:arch/mips/kernel/csrc-r4k.c
}
