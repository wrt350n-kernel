#include <linux/module.h>

#include "libgcc.h"

word_type __ucmpdi2(unsigned long long a, unsigned long long b)
{
	const DWunion au = {.ll = a};
	const DWunion bu = {.ll = b};

	if ((unsigned int) au.s.high < (unsigned int) bu.s.high)
		return 0;
	else if ((unsigned int) au.s.high > (unsigned int) bu.s.high)
		return 2;
	if ((unsigned int) au.s.low < (unsigned int) bu.s.low)
		return 0;
	else if ((unsigned int) au.s.low > (unsigned int) bu.s.low)
		return 2;
	return 1;
}
<<<<<<< HEAD:arch/mips/lib/ucmpdi2.c
=======

EXPORT_SYMBOL(__ucmpdi2);
>>>>>>> 264e3e889d86e552b4191d69bb60f4f3b383135a:arch/mips/lib/ucmpdi2.c
