<<<<<<< HEAD:arch/cris/arch-v10/lib/string.c
/*#************************************************************************#*/
/*#-------------------------------------------------------------------------*/
/*#                                                                         */
/*# FUNCTION NAME: memcpy()                                                 */
/*#                                                                         */
/*# PARAMETERS:  void* dst;   Destination address.                          */
/*#              void* src;   Source address.                               */
/*#              int   len;   Number of bytes to copy.                      */
/*#                                                                         */
/*# RETURNS:     dst.                                                       */
/*#                                                                         */
/*# DESCRIPTION: Copies len bytes of memory from src to dst.  No guarantees */
/*#              about copying of overlapping memory areas. This routine is */
/*#              very sensitive to compiler changes in register allocation. */
/*#              Should really be rewritten to avoid this problem.          */
/*#                                                                         */
/*#-------------------------------------------------------------------------*/
/*#                                                                         */
/*# HISTORY                                                                 */
/*#                                                                         */
/*# DATE      NAME            CHANGES                                       */
/*# ----      ----            -------                                       */
/*# 941007    Kenny R         Creation                                      */
/*# 941011    Kenny R         Lots of optimizations and inlining.           */
/*# 941129    Ulf A           Adapted for use in libc.                      */
/*# 950216    HP              N==0 forgotten if non-aligned src/dst.        */
/*#                           Added some optimizations.                     */
/*# 001025    HP              Make src and dst char *.  Align dst to	    */
/*#			      dword, not just word-if-both-src-and-dst-	    */
/*#			      are-misaligned.				    */
/*#                                                                         */
/*#-------------------------------------------------------------------------*/

#include <linux/types.h>

void *memcpy(void *pdst,
             const void *psrc,
             size_t pn)
=======
/* A memcpy for CRIS.
   Copyright (C) 1994-2005 Axis Communications.
   All rights reserved.

   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions
   are met:

   1. Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.

   2. Neither the name of Axis Communications nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY AXIS COMMUNICATIONS AND ITS CONTRIBUTORS
   ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL AXIS
   COMMUNICATIONS OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
   INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
   (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
   SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
   HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
   STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
   IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.  */

/* FIXME: This file should really only be used for reference, as the
   result is somewhat depending on gcc generating what we expect rather
   than what we describe.  An assembly file should be used instead.  */

#include <stddef.h>

/* Break even between movem and move16 is really at 38.7 * 2, but
   modulo 44, so up to the next multiple of 44, we use ordinary code.  */
#define MEMCPY_BY_BLOCK_THRESHOLD (44 * 2)

/* No name ambiguities in this file.  */
__asm__ (".syntax no_register_prefix");

void *
memcpy(void *pdst, const void *psrc, size_t pn)
>>>>>>> 264e3e889d86e552b4191d69bb60f4f3b383135a:arch/cris/arch-v10/lib/string.c
{
<<<<<<< HEAD:arch/cris/arch-v10/lib/string.c
  /* Ok.  Now we want the parameters put in special registers.
=======
  /* Now we want the parameters put in special registers.
>>>>>>> 264e3e889d86e552b4191d69bb60f4f3b383135a:arch/cris/arch-v10/lib/string.c
     Make sure the compiler is able to make something useful of this.
<<<<<<< HEAD:arch/cris/arch-v10/lib/string.c
      As it is now: r10 -> r13; r11 -> r11 (nop); r12 -> r12 (nop).
=======
     As it is now: r10 -> r13; r11 -> r11 (nop); r12 -> r12 (nop).
>>>>>>> 264e3e889d86e552b4191d69bb60f4f3b383135a:arch/cris/arch-v10/lib/string.c

<<<<<<< HEAD:arch/cris/arch-v10/lib/string.c
     If gcc was alright, it really would need no temporaries, and no
     stack space to save stuff on. */
=======
     If gcc was allright, it really would need no temporaries, and no
     stack space to save stuff on.  */
>>>>>>> 264e3e889d86e552b4191d69bb60f4f3b383135a:arch/cris/arch-v10/lib/string.c

  register void *return_dst __asm__ ("r10") = pdst;
<<<<<<< HEAD:arch/cris/arch-v10/lib/string.c
  register char *dst __asm__ ("r13") = pdst;
  register const char *src __asm__ ("r11") = psrc;
=======
  register unsigned char *dst __asm__ ("r13") = pdst;
  register unsigned const char *src __asm__ ("r11") = psrc;
>>>>>>> 264e3e889d86e552b4191d69bb60f4f3b383135a:arch/cris/arch-v10/lib/string.c
  register int n __asm__ ("r12") = pn;
<<<<<<< HEAD:arch/cris/arch-v10/lib/string.c
  
 
=======

>>>>>>> 264e3e889d86e552b4191d69bb60f4f3b383135a:arch/cris/arch-v10/lib/string.c
  /* When src is aligned but not dst, this makes a few extra needless
     cycles.  I believe it would take as many to check that the
     re-alignment was unnecessary.  */
  if (((unsigned long) dst & 3) != 0
      /* Don't align if we wouldn't copy more than a few bytes; so we
	 don't have to check further for overflows.  */
      && n >= 3)
  {
    if ((unsigned long) dst & 1)
<<<<<<< HEAD:arch/cris/arch-v10/lib/string.c
    {
      n--;
      *(char*)dst = *(char*)src;
      src++;
      dst++;
    }
=======
      {
	n--;
	*dst = *src;
	src++;
	dst++;
      }
>>>>>>> 264e3e889d86e552b4191d69bb60f4f3b383135a:arch/cris/arch-v10/lib/string.c

    if ((unsigned long) dst & 2)
<<<<<<< HEAD:arch/cris/arch-v10/lib/string.c
    {
      n -= 2;
      *(short*)dst = *(short*)src;
      src += 2;
      dst += 2;
    }
=======
      {
	n -= 2;
	*(short *) dst = *(short *) src;
	src += 2;
	dst += 2;
      }
>>>>>>> 264e3e889d86e552b4191d69bb60f4f3b383135a:arch/cris/arch-v10/lib/string.c
  }

<<<<<<< HEAD:arch/cris/arch-v10/lib/string.c
  /* Decide which copying method to use. */
  if (n >= 44*2)                /* Break even between movem and
                                   move16 is at 38.7*2, but modulo 44. */
  {
    /* For large copies we use 'movem' */

  /* It is not optimal to tell the compiler about clobbering any
     registers; that will move the saving/restoring of those registers
     to the function prologue/epilogue, and make non-movem sizes
     suboptimal.

      This method is not foolproof; it assumes that the "asm reg"
     declarations at the beginning of the function really are used
     here (beware: they may be moved to temporary registers).
      This way, we do not have to save/move the registers around into
     temporaries; we can safely use them straight away.

      If you want to check that the allocation was right; then
      check the equalities in the first comment.  It should say
      "r13=r13, r11=r11, r12=r12" */
    __asm__ volatile ("\n\
	;; Check that the following is true (same register names on	\n\
	;; both sides of equal sign, as in r8=r8):			\n\
	;; %0=r13, %1=r11, %2=r12					\n\
	;;								\n\
	;; Save the registers we'll use in the movem process		\n\
	;; on the stack.						\n\
	subq	11*4,$sp						\n\
	movem	$r10,[$sp]						\n\
=======
  /* Decide which copying method to use.  */
  if (n >= MEMCPY_BY_BLOCK_THRESHOLD)
    {
      /* It is not optimal to tell the compiler about clobbering any
	 registers; that will move the saving/restoring of those registers
	 to the function prologue/epilogue, and make non-movem sizes
	 suboptimal.  */
      __asm__ volatile
	("\
	 ;; GCC does promise correct register allocations, but let's	\n\
	 ;; make sure it keeps its promises.				\n\
	 .ifnc %0-%1-%2,$r13-$r11-$r12					\n\
	 .error \"GCC reg alloc bug: %0-%1-%4 != $r13-$r12-$r11\"	\n\
	 .endif								\n\
									\n\
	 ;; Save the registers we'll use in the movem process		\n\
	 ;; on the stack.						\n\
	 subq	11*4,sp							\n\
	 movem	r10,[sp]						\n\
>>>>>>> 264e3e889d86e552b4191d69bb60f4f3b383135a:arch/cris/arch-v10/lib/string.c
									\n\
<<<<<<< HEAD:arch/cris/arch-v10/lib/string.c
	;; Now we've got this:						\n\
	;; r11 - src							\n\
	;; r13 - dst							\n\
	;; r12 - n							\n\
=======
	 ;; Now we've got this:						\n\
	 ;; r11 - src							\n\
	 ;; r13 - dst							\n\
	 ;; r12 - n							\n\
>>>>>>> 264e3e889d86e552b4191d69bb60f4f3b383135a:arch/cris/arch-v10/lib/string.c
									\n\
<<<<<<< HEAD:arch/cris/arch-v10/lib/string.c
	;; Update n for the first loop					\n\
	subq	44,$r12							\n\
=======
	 ;; Update n for the first loop.				\n\
	 subq	 44,r12							\n\
>>>>>>> 264e3e889d86e552b4191d69bb60f4f3b383135a:arch/cris/arch-v10/lib/string.c
0:									\n\
<<<<<<< HEAD:arch/cris/arch-v10/lib/string.c
	movem	[$r11+],$r10						\n\
	subq	44,$r12							\n\
	bge	0b							\n\
	movem	$r10,[$r13+]						\n\
=======
"
#ifdef __arch_common_v10_v32
	 /* Cater to branch offset difference between v32 and v10.  We
	    assume the branch below has an 8-bit offset.  */
"	 setf\n"
#endif
"	 movem	[r11+],r10						\n\
	 subq	44,r12							\n\
	 bge	 0b							\n\
	 movem	r10,[r13+]						\n\
>>>>>>> 264e3e889d86e552b4191d69bb60f4f3b383135a:arch/cris/arch-v10/lib/string.c
									\n\
<<<<<<< HEAD:arch/cris/arch-v10/lib/string.c
	addq	44,$r12 ;; compensate for last loop underflowing n	\n\
=======
	 ;; Compensate for last loop underflowing n.			\n\
	 addq	44,r12							\n\
>>>>>>> 264e3e889d86e552b4191d69bb60f4f3b383135a:arch/cris/arch-v10/lib/string.c
									\n\
<<<<<<< HEAD:arch/cris/arch-v10/lib/string.c
	;; Restore registers from stack					\n\
	movem	[$sp+],$r10"
=======
	 ;; Restore registers from stack.				\n\
	 movem [sp+],r10"
>>>>>>> 264e3e889d86e552b4191d69bb60f4f3b383135a:arch/cris/arch-v10/lib/string.c

<<<<<<< HEAD:arch/cris/arch-v10/lib/string.c
     /* Outputs */ : "=r" (dst), "=r" (src), "=r" (n) 
     /* Inputs */ : "0" (dst), "1" (src), "2" (n));
    
  }
=======
	 /* Outputs.  */
	 : "=r" (dst), "=r" (src), "=r" (n)
>>>>>>> 264e3e889d86e552b4191d69bb60f4f3b383135a:arch/cris/arch-v10/lib/string.c

<<<<<<< HEAD:arch/cris/arch-v10/lib/string.c
  /* Either we directly starts copying, using dword copying
     in a loop, or we copy as much as possible with 'movem' 
     and then the last block (<44 bytes) is copied here.
     This will work since 'movem' will have updated src,dst,n. */
=======
	 /* Inputs.  */
	 : "0" (dst), "1" (src), "2" (n));
    }
>>>>>>> 264e3e889d86e552b4191d69bb60f4f3b383135a:arch/cris/arch-v10/lib/string.c

<<<<<<< HEAD:arch/cris/arch-v10/lib/string.c
  while ( n >= 16 )
  {
    *((long*)dst)++ = *((long*)src)++;
    *((long*)dst)++ = *((long*)src)++;
    *((long*)dst)++ = *((long*)src)++;
    *((long*)dst)++ = *((long*)src)++;
    n -= 16;
  }
=======
  while (n >= 16)
    {
      *(long *) dst = *(long *) src; dst += 4; src += 4;
      *(long *) dst = *(long *) src; dst += 4; src += 4;
      *(long *) dst = *(long *) src; dst += 4; src += 4;
      *(long *) dst = *(long *) src; dst += 4; src += 4;

      n -= 16;
    }
>>>>>>> 264e3e889d86e552b4191d69bb60f4f3b383135a:arch/cris/arch-v10/lib/string.c

<<<<<<< HEAD:arch/cris/arch-v10/lib/string.c
  /* A switch() is definitely the fastest although it takes a LOT of code.
   * Particularly if you inline code this.
   */
=======
>>>>>>> 264e3e889d86e552b4191d69bb60f4f3b383135a:arch/cris/arch-v10/lib/string.c
  switch (n)
<<<<<<< HEAD:arch/cris/arch-v10/lib/string.c
  {
=======
    {
>>>>>>> 264e3e889d86e552b4191d69bb60f4f3b383135a:arch/cris/arch-v10/lib/string.c
    case 0:
      break;
<<<<<<< HEAD:arch/cris/arch-v10/lib/string.c
=======

>>>>>>> 264e3e889d86e552b4191d69bb60f4f3b383135a:arch/cris/arch-v10/lib/string.c
    case 1:
<<<<<<< HEAD:arch/cris/arch-v10/lib/string.c
      *(char*)dst = *(char*)src;
=======
      *dst = *src;
>>>>>>> 264e3e889d86e552b4191d69bb60f4f3b383135a:arch/cris/arch-v10/lib/string.c
      break;
<<<<<<< HEAD:arch/cris/arch-v10/lib/string.c
=======

>>>>>>> 264e3e889d86e552b4191d69bb60f4f3b383135a:arch/cris/arch-v10/lib/string.c
    case 2:
<<<<<<< HEAD:arch/cris/arch-v10/lib/string.c
      *(short*)dst = *(short*)src;
=======
      *(short *) dst = *(short *) src;
>>>>>>> 264e3e889d86e552b4191d69bb60f4f3b383135a:arch/cris/arch-v10/lib/string.c
      break;
<<<<<<< HEAD:arch/cris/arch-v10/lib/string.c
=======

>>>>>>> 264e3e889d86e552b4191d69bb60f4f3b383135a:arch/cris/arch-v10/lib/string.c
    case 3:
<<<<<<< HEAD:arch/cris/arch-v10/lib/string.c
      *((short*)dst)++ = *((short*)src)++;
      *(char*)dst = *(char*)src;
=======
      *(short *) dst = *(short *) src; dst += 2; src += 2;
      *dst = *src;
>>>>>>> 264e3e889d86e552b4191d69bb60f4f3b383135a:arch/cris/arch-v10/lib/string.c
      break;
<<<<<<< HEAD:arch/cris/arch-v10/lib/string.c
=======

>>>>>>> 264e3e889d86e552b4191d69bb60f4f3b383135a:arch/cris/arch-v10/lib/string.c
    case 4:
<<<<<<< HEAD:arch/cris/arch-v10/lib/string.c
      *((long*)dst)++ = *((long*)src)++;
=======
      *(long *) dst = *(long *) src;
>>>>>>> 264e3e889d86e552b4191d69bb60f4f3b383135a:arch/cris/arch-v10/lib/string.c
      break;
<<<<<<< HEAD:arch/cris/arch-v10/lib/string.c
=======

>>>>>>> 264e3e889d86e552b4191d69bb60f4f3b383135a:arch/cris/arch-v10/lib/string.c
    case 5:
<<<<<<< HEAD:arch/cris/arch-v10/lib/string.c
      *((long*)dst)++ = *((long*)src)++;
      *(char*)dst = *(char*)src;
=======
      *(long *) dst = *(long *) src; dst += 4; src += 4;
      *dst = *src;
>>>>>>> 264e3e889d86e552b4191d69bb60f4f3b383135a:arch/cris/arch-v10/lib/string.c
      break;
<<<<<<< HEAD:arch/cris/arch-v10/lib/string.c
=======

>>>>>>> 264e3e889d86e552b4191d69bb60f4f3b383135a:arch/cris/arch-v10/lib/string.c
    case 6:
<<<<<<< HEAD:arch/cris/arch-v10/lib/string.c
      *((long*)dst)++ = *((long*)src)++;
      *(short*)dst = *(short*)src;
=======
      *(long *) dst = *(long *) src; dst += 4; src += 4;
      *(short *) dst = *(short *) src;
>>>>>>> 264e3e889d86e552b4191d69bb60f4f3b383135a:arch/cris/arch-v10/lib/string.c
      break;
<<<<<<< HEAD:arch/cris/arch-v10/lib/string.c
=======

>>>>>>> 264e3e889d86e552b4191d69bb60f4f3b383135a:arch/cris/arch-v10/lib/string.c
    case 7:
<<<<<<< HEAD:arch/cris/arch-v10/lib/string.c
      *((long*)dst)++ = *((long*)src)++;
      *((short*)dst)++ = *((short*)src)++;
      *(char*)dst = *(char*)src;
=======
      *(long *) dst = *(long *) src; dst += 4; src += 4;
      *(short *) dst = *(short *) src; dst += 2; src += 2;
      *dst = *src;
>>>>>>> 264e3e889d86e552b4191d69bb60f4f3b383135a:arch/cris/arch-v10/lib/string.c
      break;
<<<<<<< HEAD:arch/cris/arch-v10/lib/string.c
=======

>>>>>>> 264e3e889d86e552b4191d69bb60f4f3b383135a:arch/cris/arch-v10/lib/string.c
    case 8:
<<<<<<< HEAD:arch/cris/arch-v10/lib/string.c
      *((long*)dst)++ = *((long*)src)++;
      *((long*)dst)++ = *((long*)src)++;
=======
      *(long *) dst = *(long *) src; dst += 4; src += 4;
      *(long *) dst = *(long *) src;
>>>>>>> 264e3e889d86e552b4191d69bb60f4f3b383135a:arch/cris/arch-v10/lib/string.c
      break;
<<<<<<< HEAD:arch/cris/arch-v10/lib/string.c
=======

>>>>>>> 264e3e889d86e552b4191d69bb60f4f3b383135a:arch/cris/arch-v10/lib/string.c
    case 9:
<<<<<<< HEAD:arch/cris/arch-v10/lib/string.c
      *((long*)dst)++ = *((long*)src)++;
      *((long*)dst)++ = *((long*)src)++;
      *(char*)dst = *(char*)src;
=======
      *(long *) dst = *(long *) src; dst += 4; src += 4;
      *(long *) dst = *(long *) src; dst += 4; src += 4;
      *dst = *src;
>>>>>>> 264e3e889d86e552b4191d69bb60f4f3b383135a:arch/cris/arch-v10/lib/string.c
      break;
<<<<<<< HEAD:arch/cris/arch-v10/lib/string.c
=======

>>>>>>> 264e3e889d86e552b4191d69bb60f4f3b383135a:arch/cris/arch-v10/lib/string.c
    case 10:
<<<<<<< HEAD:arch/cris/arch-v10/lib/string.c
      *((long*)dst)++ = *((long*)src)++;
      *((long*)dst)++ = *((long*)src)++;
      *(short*)dst = *(short*)src;
=======
      *(long *) dst = *(long *) src; dst += 4; src += 4;
      *(long *) dst = *(long *) src; dst += 4; src += 4;
      *(short *) dst = *(short *) src;
>>>>>>> 264e3e889d86e552b4191d69bb60f4f3b383135a:arch/cris/arch-v10/lib/string.c
      break;
<<<<<<< HEAD:arch/cris/arch-v10/lib/string.c
=======

>>>>>>> 264e3e889d86e552b4191d69bb60f4f3b383135a:arch/cris/arch-v10/lib/string.c
    case 11:
<<<<<<< HEAD:arch/cris/arch-v10/lib/string.c
      *((long*)dst)++ = *((long*)src)++;
      *((long*)dst)++ = *((long*)src)++;
      *((short*)dst)++ = *((short*)src)++;
      *(char*)dst = *(char*)src;
=======
      *(long *) dst = *(long *) src; dst += 4; src += 4;
      *(long *) dst = *(long *) src; dst += 4; src += 4;
      *(short *) dst = *(short *) src; dst += 2; src += 2;
      *dst = *src;
>>>>>>> 264e3e889d86e552b4191d69bb60f4f3b383135a:arch/cris/arch-v10/lib/string.c
      break;
<<<<<<< HEAD:arch/cris/arch-v10/lib/string.c
=======

>>>>>>> 264e3e889d86e552b4191d69bb60f4f3b383135a:arch/cris/arch-v10/lib/string.c
    case 12:
<<<<<<< HEAD:arch/cris/arch-v10/lib/string.c
      *((long*)dst)++ = *((long*)src)++;
      *((long*)dst)++ = *((long*)src)++;
      *((long*)dst)++ = *((long*)src)++;
=======
      *(long *) dst = *(long *) src; dst += 4; src += 4;
      *(long *) dst = *(long *) src; dst += 4; src += 4;
      *(long *) dst = *(long *) src;
>>>>>>> 264e3e889d86e552b4191d69bb60f4f3b383135a:arch/cris/arch-v10/lib/string.c
      break;
<<<<<<< HEAD:arch/cris/arch-v10/lib/string.c
=======

>>>>>>> 264e3e889d86e552b4191d69bb60f4f3b383135a:arch/cris/arch-v10/lib/string.c
    case 13:
<<<<<<< HEAD:arch/cris/arch-v10/lib/string.c
      *((long*)dst)++ = *((long*)src)++;
      *((long*)dst)++ = *((long*)src)++;
      *((long*)dst)++ = *((long*)src)++;
      *(char*)dst = *(char*)src;
=======
      *(long *) dst = *(long *) src; dst += 4; src += 4;
      *(long *) dst = *(long *) src; dst += 4; src += 4;
      *(long *) dst = *(long *) src; dst += 4; src += 4;
      *dst = *src;
>>>>>>> 264e3e889d86e552b4191d69bb60f4f3b383135a:arch/cris/arch-v10/lib/string.c
      break;
<<<<<<< HEAD:arch/cris/arch-v10/lib/string.c
=======

>>>>>>> 264e3e889d86e552b4191d69bb60f4f3b383135a:arch/cris/arch-v10/lib/string.c
    case 14:
<<<<<<< HEAD:arch/cris/arch-v10/lib/string.c
      *((long*)dst)++ = *((long*)src)++;
      *((long*)dst)++ = *((long*)src)++;
      *((long*)dst)++ = *((long*)src)++;
      *(short*)dst = *(short*)src;
=======
      *(long *) dst = *(long *) src; dst += 4; src += 4;
      *(long *) dst = *(long *) src; dst += 4; src += 4;
      *(long *) dst = *(long *) src; dst += 4; src += 4;
      *(short *) dst = *(short *) src;
>>>>>>> 264e3e889d86e552b4191d69bb60f4f3b383135a:arch/cris/arch-v10/lib/string.c
      break;
<<<<<<< HEAD:arch/cris/arch-v10/lib/string.c
=======

>>>>>>> 264e3e889d86e552b4191d69bb60f4f3b383135a:arch/cris/arch-v10/lib/string.c
    case 15:
<<<<<<< HEAD:arch/cris/arch-v10/lib/string.c
      *((long*)dst)++ = *((long*)src)++;
      *((long*)dst)++ = *((long*)src)++;
      *((long*)dst)++ = *((long*)src)++;
      *((short*)dst)++ = *((short*)src)++;
      *(char*)dst = *(char*)src;
=======
      *(long *) dst = *(long *) src; dst += 4; src += 4;
      *(long *) dst = *(long *) src; dst += 4; src += 4;
      *(long *) dst = *(long *) src; dst += 4; src += 4;
      *(short *) dst = *(short *) src; dst += 2; src += 2;
      *dst = *src;
>>>>>>> 264e3e889d86e552b4191d69bb60f4f3b383135a:arch/cris/arch-v10/lib/string.c
      break;
<<<<<<< HEAD:arch/cris/arch-v10/lib/string.c
  }
=======
    }
>>>>>>> 264e3e889d86e552b4191d69bb60f4f3b383135a:arch/cris/arch-v10/lib/string.c

<<<<<<< HEAD:arch/cris/arch-v10/lib/string.c
  return return_dst; /* destination pointer. */
} /* memcpy() */
=======
  return return_dst;
}
>>>>>>> 264e3e889d86e552b4191d69bb60f4f3b383135a:arch/cris/arch-v10/lib/string.c
