<<<<<<< HEAD:arch/x86/lib/csum-wrappers_64.c
/* Copyright 2002,2003 Andi Kleen, SuSE Labs.
=======
/*
 * Copyright 2002, 2003 Andi Kleen, SuSE Labs.
>>>>>>> 264e3e889d86e552b4191d69bb60f4f3b383135a:arch/x86/lib/csum-wrappers_64.c
 * Subject to the GNU Public License v.2
<<<<<<< HEAD:arch/x86/lib/csum-wrappers_64.c
 * 
=======
 *
>>>>>>> 264e3e889d86e552b4191d69bb60f4f3b383135a:arch/x86/lib/csum-wrappers_64.c
 * Wrappers of assembly checksum functions for x86-64.
 */
<<<<<<< HEAD:arch/x86/lib/csum-wrappers_64.c

=======
>>>>>>> 264e3e889d86e552b4191d69bb60f4f3b383135a:arch/x86/lib/csum-wrappers_64.c
#include <asm/checksum.h>
#include <linux/module.h>

<<<<<<< HEAD:arch/x86/lib/csum-wrappers_64.c
/** 
 * csum_partial_copy_from_user - Copy and checksum from user space. 
 * @src: source address (user space) 
=======
/**
 * csum_partial_copy_from_user - Copy and checksum from user space.
 * @src: source address (user space)
>>>>>>> 264e3e889d86e552b4191d69bb60f4f3b383135a:arch/x86/lib/csum-wrappers_64.c
 * @dst: destination address
 * @len: number of bytes to be copied.
 * @isum: initial sum that is added into the result (32bit unfolded)
 * @errp: set to -EFAULT for an bad source address.
<<<<<<< HEAD:arch/x86/lib/csum-wrappers_64.c
 * 
=======
 *
>>>>>>> 264e3e889d86e552b4191d69bb60f4f3b383135a:arch/x86/lib/csum-wrappers_64.c
 * Returns an 32bit unfolded checksum of the buffer.
<<<<<<< HEAD:arch/x86/lib/csum-wrappers_64.c
 * src and dst are best aligned to 64bits. 
 */ 
=======
 * src and dst are best aligned to 64bits.
 */
>>>>>>> 264e3e889d86e552b4191d69bb60f4f3b383135a:arch/x86/lib/csum-wrappers_64.c
__wsum
csum_partial_copy_from_user(const void __user *src, void *dst,
			    int len, __wsum isum, int *errp)
<<<<<<< HEAD:arch/x86/lib/csum-wrappers_64.c
{ 
=======
{
>>>>>>> 264e3e889d86e552b4191d69bb60f4f3b383135a:arch/x86/lib/csum-wrappers_64.c
	might_sleep();
	*errp = 0;
<<<<<<< HEAD:arch/x86/lib/csum-wrappers_64.c
	if (likely(access_ok(VERIFY_READ,src, len))) { 
		/* Why 6, not 7? To handle odd addresses aligned we
		   would need to do considerable complications to fix the
		   checksum which is defined as an 16bit accumulator. The
		   fix alignment code is primarily for performance
		   compatibility with 32bit and that will handle odd
		   addresses slowly too. */
		if (unlikely((unsigned long)src & 6)) {			
			while (((unsigned long)src & 6) && len >= 2) { 
				__u16 val16;			
				*errp = __get_user(val16, (const __u16 __user *)src);
				if (*errp)
					return isum;
				*(__u16 *)dst = val16;
				isum = (__force __wsum)add32_with_carry(
						(__force unsigned)isum, val16);
				src += 2; 
				dst += 2; 
				len -= 2;
			}
=======

	if (!likely(access_ok(VERIFY_READ, src, len)))
		goto out_err;

	/*
	 * Why 6, not 7? To handle odd addresses aligned we
	 * would need to do considerable complications to fix the
	 * checksum which is defined as an 16bit accumulator. The
	 * fix alignment code is primarily for performance
	 * compatibility with 32bit and that will handle odd
	 * addresses slowly too.
	 */
	if (unlikely((unsigned long)src & 6)) {
		while (((unsigned long)src & 6) && len >= 2) {
			__u16 val16;

			*errp = __get_user(val16, (const __u16 __user *)src);
			if (*errp)
				return isum;

			*(__u16 *)dst = val16;
			isum = (__force __wsum)add32_with_carry(
					(__force unsigned)isum, val16);
			src += 2;
			dst += 2;
			len -= 2;
>>>>>>> 264e3e889d86e552b4191d69bb60f4f3b383135a:arch/x86/lib/csum-wrappers_64.c
		}
<<<<<<< HEAD:arch/x86/lib/csum-wrappers_64.c
		isum = csum_partial_copy_generic((__force const void *)src,
					dst, len, isum, errp, NULL);
		if (likely(*errp == 0)) 
			return isum;
	} 
=======
	}
	isum = csum_partial_copy_generic((__force const void *)src,
				dst, len, isum, errp, NULL);
	if (unlikely(*errp))
		goto out_err;

	return isum;

out_err:
>>>>>>> 264e3e889d86e552b4191d69bb60f4f3b383135a:arch/x86/lib/csum-wrappers_64.c
	*errp = -EFAULT;
<<<<<<< HEAD:arch/x86/lib/csum-wrappers_64.c
	memset(dst,0,len); 
	return isum;		
} 
=======
	memset(dst, 0, len);
>>>>>>> 264e3e889d86e552b4191d69bb60f4f3b383135a:arch/x86/lib/csum-wrappers_64.c

<<<<<<< HEAD:arch/x86/lib/csum-wrappers_64.c
=======
	return isum;
}
>>>>>>> 264e3e889d86e552b4191d69bb60f4f3b383135a:arch/x86/lib/csum-wrappers_64.c
EXPORT_SYMBOL(csum_partial_copy_from_user);

<<<<<<< HEAD:arch/x86/lib/csum-wrappers_64.c
/** 
 * csum_partial_copy_to_user - Copy and checksum to user space. 
=======
/**
 * csum_partial_copy_to_user - Copy and checksum to user space.
>>>>>>> 264e3e889d86e552b4191d69bb60f4f3b383135a:arch/x86/lib/csum-wrappers_64.c
 * @src: source address
 * @dst: destination address (user space)
 * @len: number of bytes to be copied.
 * @isum: initial sum that is added into the result (32bit unfolded)
 * @errp: set to -EFAULT for an bad destination address.
<<<<<<< HEAD:arch/x86/lib/csum-wrappers_64.c
 * 
=======
 *
>>>>>>> 264e3e889d86e552b4191d69bb60f4f3b383135a:arch/x86/lib/csum-wrappers_64.c
 * Returns an 32bit unfolded checksum of the buffer.
 * src and dst are best aligned to 64bits.
<<<<<<< HEAD:arch/x86/lib/csum-wrappers_64.c
 */ 
=======
 */
>>>>>>> 264e3e889d86e552b4191d69bb60f4f3b383135a:arch/x86/lib/csum-wrappers_64.c
__wsum
csum_partial_copy_to_user(const void *src, void __user *dst,
			  int len, __wsum isum, int *errp)
<<<<<<< HEAD:arch/x86/lib/csum-wrappers_64.c
{ 
=======
{
>>>>>>> 264e3e889d86e552b4191d69bb60f4f3b383135a:arch/x86/lib/csum-wrappers_64.c
	might_sleep();
<<<<<<< HEAD:arch/x86/lib/csum-wrappers_64.c
=======

>>>>>>> 264e3e889d86e552b4191d69bb60f4f3b383135a:arch/x86/lib/csum-wrappers_64.c
	if (unlikely(!access_ok(VERIFY_WRITE, dst, len))) {
		*errp = -EFAULT;
<<<<<<< HEAD:arch/x86/lib/csum-wrappers_64.c
		return 0; 
=======
		return 0;
>>>>>>> 264e3e889d86e552b4191d69bb60f4f3b383135a:arch/x86/lib/csum-wrappers_64.c
	}

	if (unlikely((unsigned long)dst & 6)) {
<<<<<<< HEAD:arch/x86/lib/csum-wrappers_64.c
		while (((unsigned long)dst & 6) && len >= 2) { 
=======
		while (((unsigned long)dst & 6) && len >= 2) {
>>>>>>> 264e3e889d86e552b4191d69bb60f4f3b383135a:arch/x86/lib/csum-wrappers_64.c
			__u16 val16 = *(__u16 *)src;
<<<<<<< HEAD:arch/x86/lib/csum-wrappers_64.c
=======

>>>>>>> 264e3e889d86e552b4191d69bb60f4f3b383135a:arch/x86/lib/csum-wrappers_64.c
			isum = (__force __wsum)add32_with_carry(
					(__force unsigned)isum, val16);
			*errp = __put_user(val16, (__u16 __user *)dst);
			if (*errp)
				return isum;
<<<<<<< HEAD:arch/x86/lib/csum-wrappers_64.c
			src += 2; 
			dst += 2; 
=======
			src += 2;
			dst += 2;
>>>>>>> 264e3e889d86e552b4191d69bb60f4f3b383135a:arch/x86/lib/csum-wrappers_64.c
			len -= 2;
		}
	}

	*errp = 0;
<<<<<<< HEAD:arch/x86/lib/csum-wrappers_64.c
	return csum_partial_copy_generic(src, (void __force *)dst,len,isum,NULL,errp); 
} 

=======
	return csum_partial_copy_generic(src, (void __force *)dst,
					 len, isum, NULL, errp);
}
>>>>>>> 264e3e889d86e552b4191d69bb60f4f3b383135a:arch/x86/lib/csum-wrappers_64.c
EXPORT_SYMBOL(csum_partial_copy_to_user);

<<<<<<< HEAD:arch/x86/lib/csum-wrappers_64.c
/** 
=======
/**
>>>>>>> 264e3e889d86e552b4191d69bb60f4f3b383135a:arch/x86/lib/csum-wrappers_64.c
 * csum_partial_copy_nocheck - Copy and checksum.
 * @src: source address
 * @dst: destination address
 * @len: number of bytes to be copied.
 * @isum: initial sum that is added into the result (32bit unfolded)
<<<<<<< HEAD:arch/x86/lib/csum-wrappers_64.c
 * 
=======
 *
>>>>>>> 264e3e889d86e552b4191d69bb60f4f3b383135a:arch/x86/lib/csum-wrappers_64.c
 * Returns an 32bit unfolded checksum of the buffer.
<<<<<<< HEAD:arch/x86/lib/csum-wrappers_64.c
 */ 
=======
 */
>>>>>>> 264e3e889d86e552b4191d69bb60f4f3b383135a:arch/x86/lib/csum-wrappers_64.c
__wsum
csum_partial_copy_nocheck(const void *src, void *dst, int len, __wsum sum)
<<<<<<< HEAD:arch/x86/lib/csum-wrappers_64.c
{ 
	return csum_partial_copy_generic(src,dst,len,sum,NULL,NULL);
} 
=======
{
	return csum_partial_copy_generic(src, dst, len, sum, NULL, NULL);
}
>>>>>>> 264e3e889d86e552b4191d69bb60f4f3b383135a:arch/x86/lib/csum-wrappers_64.c
EXPORT_SYMBOL(csum_partial_copy_nocheck);

__sum16 csum_ipv6_magic(const struct in6_addr *saddr,
			const struct in6_addr *daddr,
			__u32 len, unsigned short proto, __wsum sum)
{
	__u64 rest, sum64;
<<<<<<< HEAD:arch/x86/lib/csum-wrappers_64.c
     
=======

>>>>>>> 264e3e889d86e552b4191d69bb60f4f3b383135a:arch/x86/lib/csum-wrappers_64.c
	rest = (__force __u64)htonl(len) + (__force __u64)htons(proto) +
		(__force __u64)sum;
<<<<<<< HEAD:arch/x86/lib/csum-wrappers_64.c
	asm("  addq (%[saddr]),%[sum]\n"
	    "  adcq 8(%[saddr]),%[sum]\n"
	    "  adcq (%[daddr]),%[sum]\n" 
	    "  adcq 8(%[daddr]),%[sum]\n"
	    "  adcq $0,%[sum]\n"
	    : [sum] "=r" (sum64) 
	    : "[sum]" (rest),[saddr] "r" (saddr), [daddr] "r" (daddr));
	return csum_fold((__force __wsum)add32_with_carry(sum64 & 0xffffffff, sum64>>32));
}
=======
>>>>>>> 264e3e889d86e552b4191d69bb60f4f3b383135a:arch/x86/lib/csum-wrappers_64.c

<<<<<<< HEAD:arch/x86/lib/csum-wrappers_64.c
=======
	asm("	addq (%[saddr]),%[sum]\n"
	    "	adcq 8(%[saddr]),%[sum]\n"
	    "	adcq (%[daddr]),%[sum]\n"
	    "	adcq 8(%[daddr]),%[sum]\n"
	    "	adcq $0,%[sum]\n"

	    : [sum] "=r" (sum64)
	    : "[sum]" (rest), [saddr] "r" (saddr), [daddr] "r" (daddr));

	return csum_fold(
	       (__force __wsum)add32_with_carry(sum64 & 0xffffffff, sum64>>32));
}
>>>>>>> 264e3e889d86e552b4191d69bb60f4f3b383135a:arch/x86/lib/csum-wrappers_64.c
EXPORT_SYMBOL(csum_ipv6_magic);
