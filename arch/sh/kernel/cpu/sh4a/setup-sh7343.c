/*
 * SH7343 Setup
 *
 *  Copyright (C) 2006  Paul Mundt
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License.  See the file "COPYING" in the main directory of this archive
 * for more details.
 */
#include <linux/platform_device.h>
#include <linux/init.h>
#include <linux/serial.h>
<<<<<<< HEAD:arch/sh/kernel/cpu/sh4a/setup-sh7343.c
#include <asm/sci.h>
=======
#include <linux/serial_sci.h>
>>>>>>> 264e3e889d86e552b4191d69bb60f4f3b383135a:arch/sh/kernel/cpu/sh4a/setup-sh7343.c

static struct plat_sci_port sci_platform_data[] = {
	{
		.mapbase	= 0xffe00000,
		.flags		= UPF_BOOT_AUTOCONF,
		.type		= PORT_SCIF,
		.irqs		= { 80, 81, 83, 82 },
	}, {
		.flags = 0,
	}
};

static struct platform_device sci_device = {
	.name		= "sh-sci",
	.id		= -1,
	.dev		= {
		.platform_data	= sci_platform_data,
	},
};

static struct platform_device *sh7343_devices[] __initdata = {
	&sci_device,
};

static int __init sh7343_devices_setup(void)
{
	return platform_add_devices(sh7343_devices,
				    ARRAY_SIZE(sh7343_devices));
}
__initcall(sh7343_devices_setup);

void __init plat_irq_setup(void)
{
}
