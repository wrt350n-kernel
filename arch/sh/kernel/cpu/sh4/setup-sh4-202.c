/*
 * SH4-202 Setup
 *
 *  Copyright (C) 2006  Paul Mundt
 *
 * This file is subject to the terms and conditions of the GNU General Public
 * License.  See the file "COPYING" in the main directory of this archive
 * for more details.
 */
#include <linux/platform_device.h>
#include <linux/init.h>
#include <linux/serial.h>
<<<<<<< HEAD:arch/sh/kernel/cpu/sh4/setup-sh4-202.c
#include <asm/sci.h>
=======
#include <linux/serial_sci.h>
>>>>>>> 264e3e889d86e552b4191d69bb60f4f3b383135a:arch/sh/kernel/cpu/sh4/setup-sh4-202.c

static struct plat_sci_port sci_platform_data[] = {
	{
		.mapbase	= 0xffe80000,
		.flags		= UPF_BOOT_AUTOCONF,
		.type		= PORT_SCIF,
		.irqs		= { 40, 41, 43, 42 },
	}, {
		.flags = 0,
	}
};

static struct platform_device sci_device = {
	.name		= "sh-sci",
	.id		= -1,
	.dev		= {
		.platform_data	= sci_platform_data,
	},
};

static struct platform_device *sh4202_devices[] __initdata = {
	&sci_device,
};

static int __init sh4202_devices_setup(void)
{
	return platform_add_devices(sh4202_devices,
				    ARRAY_SIZE(sh4202_devices));
}
__initcall(sh4202_devices_setup);

void __init plat_irq_setup(void)
{
	/* do nothing - all IRL interrupts are handled by the board code */
}
