/*
 * arch/arm/mach-orion/dns323-setup.c
 *
 * Copyright (C) 2007 Herbert Valerio Riedel <hvr@gnu.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 */

#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/platform_device.h>
#include <linux/pci.h>
#include <linux/irq.h>
#include <linux/mtd/physmap.h>
#include <linux/mv643xx_eth.h>
#include <linux/leds.h>
#include <linux/gpio_keys.h>
#include <linux/input.h>
#include <linux/i2c.h>
#include <asm/mach-types.h>
#include <asm/gpio.h>
#include <asm/mach/arch.h>
#include <asm/mach/pci.h>
#include <asm/arch/orion.h>
#include <asm/arch/platform.h>
#include "common.h"


/****************************************************************************
 * Ethernet
 */

static struct mv643xx_eth_platform_data wrt350n_v2_eth_data = {
	.phy_addr = 0,
};

/****************************************************************************
 * 8MiB NOR flash 
 *
 * Layout as used by Linksys:
 *  0x00000000-0x00760000 : "kernel"
 *  0x001a0000-0x00760000 : "rootfs"
 *  0x00760000-0x007a0000 : "lang"
 *  0x007a0000-0x007c0000 : "nvram"
 *  0x007c0000-0x00800000 : "u-boot"
*/

#define WRT350N_V2_NOR_BOOT_BASE 0xf4000000
#define WRT350N_V2_NOR_BOOT_SIZE SZ_8M

static struct mtd_partition wrt350n_v2_partitions[] = {
	{
		.name	= "kernel",
		.size	= 0x00760000,
		.offset	= 0,
	}, {
		.name	= "rootfs",
		.size	= 0x005c0000,
		.offset = 0x001a0000,
	}, {
		.name	= "lang",
		.size	= 0x00040000,
		.offset	= 0x00760000,
	}, {
		.name	= "nvram",
		.size	= 0x00020000,
		.offset	= 0x007a0000,
	}, {
		.name	= "u-boot",
		.size	= 0x00040000,
		.offset	= 0x007c0000,
	}
};

static struct physmap_flash_data wrt350n_v2_nor_flash_data = {
	.width		= 1,
	.parts		= wrt350n_v2_partitions,
	.nr_parts	= ARRAY_SIZE(wrt350n_v2_partitions)
};

static struct resource wrt350n_v2_nor_flash_resource = {
	.flags		= IORESOURCE_MEM,
	.start		= WRT350N_V2_NOR_BOOT_BASE,
	.end		= WRT350N_V2_NOR_BOOT_BASE + WRT350N_V2_NOR_BOOT_SIZE - 1,
};

static struct platform_device wrt350n_v2_nor_flash = {
	.name		= "physmap-flash",
	.id		= 0,
	.dev		= { .platform_data = &wrt350n_v2_nor_flash_data, },
	.resource	= &wrt350n_v2_nor_flash_resource,
	.num_resources	= 1,
};



/**************************************************************************
 * General Setup
 */

static struct platform_device *wrt350n_v2_plat_devices[] __initdata = {
	&wrt350n_v2_nor_flash,
};

static void __init wrt350n_v2_init(void)
{
	/* Setup basic Orion functions. Need to be called early. */
	orion_init();

	/* setup flash mapping
	 */
	orion_setup_cpu_win(ORION_DEV_BOOT, WRT350N_V2_NOR_BOOT_BASE,
			    WRT350N_V2_NOR_BOOT_SIZE, -1);


	/* set MPP as Linksys's kernel did */
	orion_write(MPP_0_7_CTRL, 0x00000003);
	orion_write(MPP_8_15_CTRL, 0x00000101);
	orion_write(MPP_16_19_CTRL, 0);
	orion_write(MPP_DEV_CTRL, 0);

	/* register flash and other platform devices */
	platform_add_devices(wrt350n_v2_plat_devices,
			     ARRAY_SIZE(wrt350n_v2_plat_devices));
	orion_eth_init(&wrt350n_v2_eth_data);
}


MACHINE_START(WRT350N_V2, "Linksys WRT350N v2")
	/* Maintainer: No-one yet*/
	.phys_io	= ORION_REGS_PHYS_BASE,
	.io_pg_offst	= ((ORION_REGS_VIRT_BASE) >> 18) & 0xFFFC,
	.boot_params	= 0x00000100,
	.init_machine	= wrt350n_v2_init,
	.map_io		= orion_map_io,
	.init_irq	= orion_init_irq,
	.timer		= &orion_timer,
MACHINE_END
